#!/usr/bin/python
# sshfs ceres@10.42.0.91:/home/ceres /home/vinod/ceres-remote
import rospy
import actionlib

from hub.msg import CaptureAction, CaptureGoal
from hub.msg import DetectAction, DetectGoal
from std_msgs.msg import String

class Hub_State:
    IDLE = 0
    CAPTURE = 1
    DETECT = 2
    UPDATE = 3

class HubClient(object):
    def __init__(self):
        self.door_status = None
        self.hub_state = Hub_State.IDLE
        self.capture = actionlib.SimpleActionClient("cam_server", CaptureAction)
        self.detect = actionlib.SimpleActionClient("detect_server", DetectAction)
        self.connect()        
        rospy.Subscriber("door_activity", String, self.door_cb)
        self.spin()

    def door_cb(self, data):
        print(data)
        self.hub_state = Hub_State.CAPTURE

    def connect(self):
        self.capture.wait_for_server()
        self.detect.wait_for_server()

    def feedback_capture_cb(self, msg):
        print("Capture Feedback ", msg)

    def feedback_detect_cb(self, msg):
        print("Detect Feedback ", msg)

    def spin(self):
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            if self.hub_state == Hub_State.CAPTURE:
                goal = CaptureGoal()
                goal.shelves = ["1"]

                self.capture.send_goal(goal, feedback_cb=self.feedback_capture_cb)
                self.capture.wait_for_result()
                result = self.capture.get_result()
                if result.result_message == "Capture Complete":
                    self.hub_state = Hub_State.DETECT
                    print("Capture Result: ", result)
                elif result.result_message == "Capture Failed":
                    print("Hub Idle")
                    self.hub_state = Hub_State.IDLE
            elif self.hub_state == Hub_State.DETECT:
                goal = DetectGoal()
                goal.shelves = ["1"]

                self.detect.send_goal(goal, feedback_cb=self.feedback_detect_cb)
                self.detect.wait_for_result()
                result = self.detect.get_result()
                if result.result_message == "Detect Complete":
                    self.hub_state = Hub_State.UPDATE
                    print("Detect Result: ", result)

            elif self.hub_state == Hub_State.UPDATE:
                    
                    self.hub_state = Hub_State.IDLE
                    print("Update Result")
            else:
                print("Idle")
                rate.sleep()

if __name__ == "__main__":

    try:
        rospy.init_node("hub_client")
        c = HubClient()        
    except rospy.ROSInterruptException as e:
        print("Something went wrong ", e)

