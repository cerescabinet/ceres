#!/usr/bin/python

import rospy
from std_msgs.msg import String
from hub.msg import Door

class Door_State:    
    DOOR_CLOSE = 0
    DOOR_OPEN = 1

def door():
    pub = rospy.Publisher('door_activity', String, queue_size=10)
    rospy.init_node('door', anonymous=True)
    rate = rospy.Rate(1)
    
    door_state = None
    while not rospy.is_shutdown():
        try:
            f = open("/home/ceres/catkin_ws/src/hub/scripts/switch.txt")
            status = f.read(1)
        finally:
            f.close()
        print("Status ", status)
        if status == '0' and door_state == Door_State.DOOR_OPEN:
            door_state = Door_State.DOOR_CLOSE
            # publish
            print("publising")
            door_msg = Door()
            door_msg.shelves = ["1", "2"] # Not using for the moment
            pub.publish("1")
        elif status == '1':
            door_state = Door_State.DOOR_OPEN
        else:
            door_state = Door_State.DOOR_CLOSE

        rate.sleep()

if __name__ == '__main__':
    try:
        door()
    except rospy.ROSInterruptException:
        pass
