#!/usr/bin/python

import rospy
import actionlib
from hub.msg import CaptureAction, CaptureFeedback, CaptureResult
import cv2
import numpy as np
import glob
import RPi.GPIO as gp
import os

gp.setwarnings(False)
gp.setmode(gp.BOARD)
gp.setup(7, gp.OUT)
gp.setup(11, gp.OUT)
gp.setup(12, gp.OUT)

def gstreamer_pipeline(
    capture_width=3280,
    capture_height=2464,
    display_width=3280,
    display_height=2464,
    framerate=1,
    flip_method=0,
):
    return (
        "nvarguscamerasrc ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )


class CamServer(object):
    def __init__(self):
        self.c_server = actionlib.SimpleActionServer("cam_server", CaptureAction, execute_cb=self.exec_cb, auto_start = False)
        self.c_server.start()

    def capture(self, shelf, side, x_crop, y_crop):
        parameters = np.load("/home/ceres/catkin_ws/src/hub/scripts/calib.npz")
        mtx = parameters["mtx"]
        dist = parameters["dist"]
        rvecs = parameters["rvecs"]
        tvecs = parameters["tvecs"]

        # naming images
        # import time
        # timestr = time.strftime("%Y%m%d-%H%M%S")

        # Deleting oldest files
        # list_of_files = os.listdir('log')
        # full_path = ["log/{0}".format(x) for x in list_of_files]

        # if len([name for name in list_of_files]) == 25:
        #     oldest_file = min(full_path, key=os.path.getctime)
        #     os.remove(oldest_file)

        # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
        # print(gstreamer_pipeline(flip_method=0))
        
        cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
        if cap.isOpened():
            ret_val, img = cap.read()
            h, w = img.shape[:2]
            newCameraMtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
            undistortedImg = cv2.undistort(img, mtx, dist, None, newCameraMtx)
            cropped_image = undistortedImg[x_crop:x_crop + 1376, y_crop:y_crop + 1376]
            cv2.imwrite("/home/ceres/catkin_ws/src/hub/imgs/shelf_%d_%s.jpg" % (shelf, side), cropped_image)
            cap.release()
            return True
        else:
            print("Unable to open camera") 
            return False


    def exec_cb(self, goal):
        print("Recieved Camera goal: ", goal)
        # rate = rospy.Rate(1)
        feedback = CaptureFeedback()
        result = CaptureResult()

        i2c = "i2cset -y 1 0x70 0x00 0x04"
        os.system(i2c)
        gp.output(7, False)
        gp.output(11, False)
        gp.output(12, True)
        success = self.capture(1, "left", 558, 700)

        i2c = "i2cset -y 1 0x70 0x00 0x06"
        os.system(i2c)
        gp.output(7, False)
        gp.output(11, True)
        gp.output(12, False)
        success = self.capture(1, "right", 558, 1250)
        
        # Reset GPIO
        gp.output(7, False)
        gp.output(11, False)
        gp.output(12, True)

        feedback.feedback_message = "Still capturing"

        self.c_server.publish_feedback(feedback)

        if self.c_server.is_preempt_requested():
            success = False

        if success:
            result.result_message = "Capture Complete"
            self.c_server.set_succeeded(result)
        else:
            result.result_message = "Capture Failed"
            self.c_server.set_succeeded(result)

if __name__ == "__main__":
    rospy.init_node("cam_server")
    s = CamServer()
    rospy.spin()