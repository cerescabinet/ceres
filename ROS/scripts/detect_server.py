#!/usr/bin/python

import rospy
import actionlib
from hub.msg import DetectAction, DetectFeedback, DetectResult

class DetectServer(object):
    def __init__(self):
        self.d_server = actionlib.SimpleActionServer("detect_server", DetectAction, execute_cb=self.exec_cb, auto_start = False)
        self.d_server.start()

    def exec_cb(self, goal):
        print(goal)
        rate = rospy.Rate(1)
        feedback = DetectFeedback()
        result = DetectResult()

        success = True

        for i in range(10):
            if self.d_server.is_preempt_requested():
                success = False
                print("break")
                break
            feedback.feedback_message = "Detect Feedback "+str(i)
            result.result_message = "Detect Complete"
            self.d_server.publish_feedback(feedback)
            rate.sleep()
        if success:
            self.d_server.set_succeeded(result)

if __name__ == "__main__":
    rospy.init_node("detect_server")
    s = DetectServer()
    rospy.spin()