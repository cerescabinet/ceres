#!/usr/bin/python

import rospy
import actionlib
from hub.msg import CaptureAction, CaptureFeedback, CaptureResult
from std_msgs.msg import String

class Door_State:    
    DOOR_OPEN = 0
    DOOR_CLOSE = 1


class HubServer(object):
    def __init__(self):
        self.

        self.h_server = actionlib.SimpleActionServer("hub_server", HubAction, execute_cb=self.exec_cb, auto_start = False)
        self.h_server.start()

        self.capture_client = actionlib.SimpleActionClient("cam_server", CaptureAction)
        capture_client.wait_for_server()
        goal = CaptureGoal()

        rospy.Subscriber("door_activity", String, self.door_cb)        

    def door_cb(self, data):
        print('Door Activity')

    def exec_cb(self, goal):
        print(goal)
        rate = rospy.Rate(1)
        feedback = CaptureFeedback()
        result = CaptureResult()

        success = True

        if 


        for i in range(10):
            if self.h_server.is_preempt_requested():
                success = False
                break
            feedback.feedback_message = "Capture Feedback "+str(i)
            result.result_message = "Caapture Result"
            self.h_server.publish_feedback(feedback)
            rate.sleep()
        if success:
            self.h_server.set_succeeded(result)


if __name__ == "__main__":
    rospy.init_node("hub_server")    
    s = HubServer()
    rospy.Subscriber("door_activity", String, door_cb)
    rospy.spin()