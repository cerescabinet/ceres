import matplotlib.pyplot as plt
import numpy as np

# r1 = [600, 440, 320, 230, 170, 140, 120, 103, 91, 84, 76, 72, 70, 69, 68]
# r2 = 330
# vout = []
# for r in r1:
#     vout.append((5 * r2)/(r + r2))

# plt.plot(vout)
# plt.show()
# print(vout)

a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]
a = np.array(a)
mask = [[False, True, True],
        [False, True, True],
        [False, True, True]]

b = a[np.array(mask)]
print(b)

'''
def find_blobs(img):
    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()
     
    # Change thresholds
    params.minThreshold = 100
    params.maxThreshold = 5000
     
    # Filter by Area.
    params.filterByArea = True
    params.minArea = 200
     
    # Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.1
     
    # Filter by Convexity
    params.filterByConvexity = True
    params.minConvexity = 0.87
     
    # Filter by Inertia
    #params.filterByInertia = True
    #params.minInertiaRatio = 0.01

    # Set up the detector with default parameters.
    detector = cv2.SimpleBlobDetector_create(params)
     
    # Detect blobs.
    keypoints = detector.detect(img)
      
    # Draw detected blobs as red circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
    im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]),
            (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imwrite("blobs.jpg", im_with_keypoints); 
    '''