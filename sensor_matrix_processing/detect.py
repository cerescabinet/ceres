# import the necessary packages
import cv2
import argparse
import imutils
import cv2
import numpy as np

class ShapeDetector:
    def __init__(self):
        pass
    def detect(self, c):
        # initialize the shape name and approximate the contour
        shape = "unidentified"
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)

        # if the shape is a triangle, it will have 3 vertices
        if len(approx) == 3:
            shape = "triangle"
        # if the shape has 4 vertices, it is either a square or
        # a rectangle
        elif len(approx) == 4:
            # compute the bounding box of the contour and use the
            # bounding box to compute the aspect ratio
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)
            # a square will have an aspect ratio that is approximately
            # equal to one, otherwise, the shape is a rectangle
            shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
        # if the shape is a pentagon, it will have 5 vertices
        elif len(approx) == 5:
            shape = "pentagon"
        # otherwise, we assume the shape is a circle
        else:
            shape = "circle"
        print(shape)

        cX = approx.ravel()[0]
        cY = approx.ravel()[1]

        # return the name of the shape
        return shape, cX, cY


if __name__ == '__main__':
    image = cv2.imread("data/box_small.png", cv2.IMREAD_GRAYSCALE)
    mat = np.array(image)
    blurred = cv2.GaussianBlur(image, (1, 1), 0)
    image = cv2.threshold(blurred, 6, 255, cv2.THRESH_BINARY)[1]

    cnts = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    sd = ShapeDetector()

    # (x,y),radius = cv2.minEnclosingCircle(cnts[0])
    # center = (int(x),int(y))
    # radius = int(radius)
    # cv2.circle(image, center, radius, (0,255,0), 1)
    # cv2.imshow("circle", image)
    # cv2.waitKey(0)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    for c in cnts:
        area = cv2.contourArea(c)
        if area < 20:
            print("pass")
            continue
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
        M = cv2.moments(c)


        # cX = int((M["m10"] / M["m00"]) )
        # cY = int((M["m01"] / M["m00"]) )
        shape, cX, cY = sd.detect(c)
        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
        #c = c.astype("float")
        #c *= ratio
        c = c.astype("int")
        
        # cv2.drawContours(image , [c], -1, (0, 255, 0), 2)
        # cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        # show the output image
        # cv2.imshow("Image", image)
        x,y,w,h = cv2.boundingRect(c)



        cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),1)

        cv2.imshow("thresh", image)
        cv2.waitKey(0)
        cv2.imwrite("shape_of_you.png",image)
        # cv2.waitKey(0)