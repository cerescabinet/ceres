from __future__ import print_function
import serial, os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pickle
import time

ser = serial.Serial('/dev/ttyACM0', 500000)

w, h = 16, 16
calib = None
with open("calib_1k_raw.pkl","rb") as f:
	calib = pickle.load(f)

# print(calib)	

matrix = np.zeros((w, h), dtype=int)

prev_total_weight = 0
WEIGHT_REDUCED = False
WEIGHT_INCREASED = False

def generate_data():
	while not ord(ser.read()) == 0:
		pass
	for y in range(h):
		row = np.zeros(w, dtype=int)	
		for x in range(w):
			readByte = ser.read()
			if  ord(readByte)==0:
				break
			else:
				row[x] = ord(readByte)
		readByte = ser.read()	
		matrix[ord(readByte)] = row - calib[y]
	
	# with open("calib.pkl","wb") as f:
	# 	pickle.dump(matrix, f)		

	print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in matrix]) , sum(matrix))

	return matrix

def update(data):
    mat.set_data(data)
    return mat
	
def data_gen():
	while True:
		yield generate_data()
		
fig, ax = plt.subplots()
mat = ax.matshow(generate_data(),  vmin=0, vmax=100)
ax.autoscale(False)
plt.colorbar(mat)
ani = animation.FuncAnimation(fig, update, data_gen)

plt.show()

# data_gen()