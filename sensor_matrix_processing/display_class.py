from __future__ import print_function
import serial, os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pickle
import time
import cv2
from PIL import Image
import imutils
from datetime import datetime

class Sense(object):
	W, H = 16, 16
	calib = None
	
	def __init__(self):
		self.matrix = np.zeros((self.W, self.H), dtype=int)
		self.current_total_weight = 0
		self.prev_total_weight = 0

		with open("calib_1k_raw.pkl","rb") as f:
			self.calib = pickle.load(f)
		
	def fetch(self, ser):
		while not ord(ser.read()) == 0:
			pass
		for y in range(self.H):
			row = np.zeros(self.W, dtype=int)	
			for x in range(self.W):
				readByte = ser.read()
				if  ord(readByte)==0:
					break
				else:
					row[x] = ord(readByte)
			readByte = ser.read()
			index = ord(readByte)
			if index != y:
				print("COM: Lost lines")
				return False
			self.matrix[index] = row - self.calib[y]		
		# with open("calib_1k_raw.pkl","wb") as f:
		# 	pickle.dump(self.matrix, f)				
		return True
	
	def detect(self, c):
		# initialize the shape name and approximate the contour
		shape = "unidentified"
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.04 * peri, True)

		# if the shape is a triangle, it will have 3 vertices
		if len(approx) == 3:
			shape = "triangle"
		# if the shape has 4 vertices, it is either a square or
		# a rectangle
		elif len(approx) == 4:
			# compute the bounding box of the contour and use the
			# bounding box to compute the aspect ratio
			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w / float(h)
			# a square will have an aspect ratio that is approximately
			# equal to one, otherwise, the shape is a rectangle
			shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
		# if the shape is a pentagon, it will have 5 vertices
		elif len(approx) == 5:
			shape = "pentagon"
		# otherwise, we assume the shape is a circle
		else:
			shape = "circle"
		print(shape)

		cX = approx.ravel()[0]
		cY = approx.ravel()[1]

		# return the name of the shape
		return shape, cX, cY

	def process(self):
		# for some reason first two reads were not proper. data seemed incomplete. So keeping third one.
		ser = serial.Serial('/dev/ttyACM0', 500000)
		retry = 1
		fetched = self.fetch(ser)
		while not fetched and retry <= 3:
			print("COM error, Retry ", retry)
			fetched = self.fetch(ser)
			retry += 1

		if retry == 5:
			return False

		self.matrix[self.matrix < 0] = 0
		self.matrix[self.matrix > 255] = 255
		print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in self.matrix]) , sum(self.matrix))
		# remove the top row because of unusual misreadings on it
		mat = self.matrix[1:]
		mat = np.reshape(mat.astype(np.uint8),(15,16))
		thresh = cv2.threshold(mat, 30, 255, cv2.THRESH_BINARY)[1]
		# cv2.imshow("thresh", mat)
		# cv2.waitKey(0)
		# return
		# image = Image.fromarray(mat,  mode='L')
		cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		print("contours: ", len(cnts))
		total_pressure = 0
		for c in cnts:
			area = cv2.contourArea(c)
			print("Area ", area)
			if area < 10.0:
				print("pass")
				continue
			# shape, cX, cY = self.detect(c)
			x,y,w,h = cv2.boundingRect(c)
			cont_array = np.array(cv2.drawContours(mat.copy() , [c], -1, (255, 255, 255), -1))
			total_pressure = sum(mat[cont_array > 254])
			# cv2.rectangle(mat,(x,y),(x+w,y+h),(255,255,255),1)
			# cv2.imshow("thresh", cont_array)
			# cv2.waitKey(0)
			print("{:d} Rows {:d} Colums , Area: {:2f} sum {:d}".format(x, y, area, total_pressure))
			
		# print(masked)
		# cv2.imshow("thresh", mat)
		# cv2.waitKey(0)
		# cv2.imwrite("data/box_half_small.png",thresh)

		# find_blobs(img)

		return True, total_pressure

if __name__ == '__main__':
	pressures = []
	while True:
		now = datetime.now()
		current_time = now.strftime("%H:%M:%S")
		print("Current Time =", current_time)
		sense = Sense()
		status, pressure = sense.process()
		if not status:
			print("Major COM failure")
		time.sleep(0.5)
		if pressure!=0:
			pressures.append(pressure)
		print(pressures)

'''
		img = Image.fromarray(mat,  mode='L')
		# img = img.resize((150, 160), Image.ANTIALIAS)
		# img = np.array(img)


'''			