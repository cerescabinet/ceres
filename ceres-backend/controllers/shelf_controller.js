const express = require("express");
const CabinetShelfModel = require("../models/cabinet_shelf_model");
const CabinetItemModel = require("../models/cabinet_item_model");
const multer = require("multer");
const bodyParser = require("body-parser");
const path = require("path");
const UploadHelper = require("../utils/upload_helper");
const Constants = require("../utils/constants");
const PusherHelper = require("../utils/pusher_helper");
const pusherObj = new PusherHelper(Constants.getDashboardPusherConfig());

/**
 * Controller for home page
 */
module.exports.getSimulatorHome = function (request, response) {
  let items_arr = [];
  let locals = { title: "Simulator Home", items_arr: items_arr };

  CabinetShelfModel.find({}, function (error, items) {
    if (error) {
      console.log("Error in fetching items from DB");
      return;
    }
    for (let item of items) {
      let currentItem = createShelfItemObj(item, true);
      items_arr.push(currentItem);
    }
    return response.render("simulator", locals);
  });
};

/**
 * Helper method to create ShelfItemObj by parsing the request / parsing item returned by the DB
 * @param {*} item
 */
const createShelfItemObj = (item) => {
  let currentItem = {
    shelf_id: item.shelf_id,
    shelf_side: item.shelf_side,
    id: item._id,
  };
  return currentItem;
};

/**
 * Helper function to set file desitnation in fileProperties object to be used later for creating entry in the DB
 * @param {*} folderName - name of the destination folder to save the uploaded file
 * @param {*} fileProperties - object to be updated with the desitnation path
 */
function setFileDestination(folderName, fileProperties) {
  fileProperties["destination"] = UploadHelper.getFileStoragePath(folderName);
  return fileProperties["destination"];
}

/**
 * helper function to set file name in fileProperties object to be used later for creating entry in the DB
 * @param {*} fileProperties - object that is to be updated with file_name
 * @param {*} shelf_id - shelf_id of the uploaded shelf image
 * @param {*} shelf_side - shelf_side of the uploaded shelf image [left, right]
 * @param {*} file - uploaded file
 */
function setFileProperties(fileProperties, shelf_id, shelf_side, file) {
  fileProperties["file_name"] = `shelf_${shelf_id}_${shelf_side}${path.extname(
    file.originalname
  )}`;
  return fileProperties["file_name"];
}

/**
 * Controller for adding / updating a shelf image
 * Handles uploading of image
 * Requries "shelf_id" and "shelf_side" for each image
 */
module.exports.addShelfItem = function (request, response) {
  let fileProperties = {};

  // set storage engine with desitnation and filename - TODO: add limits like file-size
  const storageEngine = multer.diskStorage({
    destination: function (request, file, callback) {
      callback(null, setFileDestination("shelf", fileProperties));
    },
    filename: function (request, file, callback) {
      callback(
        null,
        setFileProperties(
          fileProperties,
          request.body.shelf_id,
          request.body.shelf_side,
          file
        )
      );
    },
  });

  // Initialize Upload variable
  const upload = multer({
    storage: storageEngine,
    limits: {
      fileSize: Constants.getImageFileUploadSizeLimit(), // in bytes
    },
    fileFilter: function (request, file, callback) {
      UploadHelper.checkImageFileType(file, callback);
    },
  }).single(Constants.getImageFieldName());

  // call upload method
  upload(request, response, (error) => {
    if (error) {
      console.log("error", error);
      response.status(400);
      return response.send(error);
    } else {
      // if file is not present
      if (request.file == undefined) {
        response.status(400);
        return response.send("No File Selected");
      }
      // if file is present
      else {
        let shelf_id = request.body.shelf_id;
        let shelf_side = request.body.shelf_side;

        // check if file parameters exists in the DB
        CabinetShelfModel.findOne(
          {
            shelf_id: shelf_id,
            shelf_side: shelf_side,
          },
          function (err, obj) {
            if (err) {
              console.log("error querying document");
            } else {
              // if file parameters do not exist in the DB
              if (obj == null) {
                console.log("Object does not exist. Adding it to the DB");
                // TODO: change this into a function
                CabinetShelfModel.create(
                  {
                    shelf_id: request.body.shelf_id,
                    shelf_side: request.body.shelf_side,
                    image_path: path.join(
                      fileProperties["destination"],
                      fileProperties["file_name"]
                    ),
                  },
                  function (error, newItem) {
                    if (error) {
                      console.log("Error creating model", error);
                      response.status(400);
                      return response.send(error);
                    } else {
                      // trigger a Pushser message to the client that an image has been added in the shelf
                      pusherObj.triggerMessage(
                        Constants.getDashboadPusherChannelName(),
                        Constants.getDashboardPusherEventName(),
                        newItem
                      );

                      let shelfItem = createShelfItemObj(newItem);
                      response.status(200);
                      return response.json(shelfItem);
                    }
                  }
                );
              }
              // if file parameters exist in the DB
              else {
                console.log("The shelf was initialized earlier. Only updating the file.");
                // trigger a Pushser message to the client that an image has updated in the shelf
                pusherObj.triggerMessage(
                  Constants.getDashboadPusherChannelName(),
                  Constants.getDashboardPusherEventName(),
                  {
                    shelf_id: shelf_id,
                    shelf_side: shelf_side,
                  }
                );
                response.status(204);
                return response.send("");
              }
            }
          }
        );
      }
    }
  });
};

/**
 * Controller function to delete a shelf-image
 * Requires id of the DB entry - the same id is used in HTML on the client side
 */
module.exports.deleteShelfItems = function (request, response) {
  // create ids array to delete multiple items based on id
  let ids = [];
  for (let item of request.body.data) {
    ids.push(item.id);
  }

  let deletedIds = {
    _id: { $in: ids },
  };

  // delete multiple items
  CabinetShelfModel.deleteMany(deletedIds, function (error, result) {
    if (error) {
      console.log("Failed to delete the records from the DB");
      response.status(400);
      return response.send(error);
    } else {
      pusherObj.triggerMessage(
        Constants.getDashboadPusherChannelName(),
        Constants.getDashboardPusherEventName(),
        deletedIds
      );
    }
  });
  return response.redirect("back");
};

/**
 * Helper function to parse item information from the request object
 * and prepare object (to be later added / updated in the DB by the main function)
 * @param {*} json_obj --> this object must have item information including the item_id
 */
function prepareItemsDocument(json_obj) {
  let items_arr = [];

  for (let item of json_obj["shelf"]) {
    for (let current_item of item.items) {
      let current_obj = {};
      current_obj.item_id = current_item.item_id;
      current_obj.item_shelf_id = item.shelf_id;
      current_obj.item_side = item.side;
      current_obj.item_category = current_item.category;
      current_obj.item_brand = current_item.brand;
      current_obj.item_full_weight = current_item.full_weight;
      current_obj.item_current_weight = current_item.current_weight;

      let item_location = {};
      item_location.x = parseFloat(current_item.location[0].x);
      item_location.y = parseFloat(current_item.location[0].y);
      item_location.h = parseFloat(current_item.location[0].h);
      item_location.w = parseFloat(current_item.location[0].w);
      current_obj.item_location = item_location;
      items_arr.push(current_obj);
    }
  }
  return items_arr;
}

/**
 * Controller function to handle /add-items route
 * Adds / updates item information in cabinet_items DB
 * Performs upsert information
 */
module.exports.addItems = function (request, response) {
  let itemsArr = prepareItemsDocument(request.body);
  for (let item of itemsArr) {
    CabinetItemModel.findOneAndUpdate(
      { item_id: item.item_id }, // find a document with that filter
      item, // document to insert when nothing was found
      { upsert: true, new: true, runValidators: true }, // options
      function (err, doc) {
        // callback
        if (err) {
          console.log(
            `Error upserting the document to CabinetItemModel: ${err}`
          );
        } else {
          console.log(doc);
        }
      }
    );
  }
  response.status(200);
  return response.send("success");
};

/**
 * Controller function to handle /get-all-items route
 * Returns all items present in the cabinet_items DB as JSON
 */
module.exports.getAllItems = function (request, response) {
  CabinetItemModel.find({}, function (error, items) {
    if (error) {
      console.log(`Error querying all items from the DB: ${error}`);
      response.status(400);
      return response.send(`${error}`);
    }
    response.status(200);
    return response.json(items);
  });
};

module.exports.getImageItemInfo = function (request, response) {
  let shelfId = request.query.shelfId;
  let shelfSide = request.query.shelfSide;
  console.log("Received shelfId: ", shelfId, " shelfSide: ", shelfSide);
  CabinetItemModel.find(
    { item_shelf_id: shelfId, item_side: shelfSide },
    function (error, items) {
      if (error) {
        console.log(`Error querying all items from the DB: ${error}`);
        response.status(400);
        return response.send(`${error}`);
      } else {
        response.status(200);
        console.log(items);
        return response.json(items);
      }
    }
  );
};
