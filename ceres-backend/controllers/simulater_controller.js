const express = require("express");
const CabinetItemModel = require("../models/cabinet_item_model");
const multer = require("multer");
const bodyParser = require('body-parser');
const path = require("path");
const UploadHelper = require("../utils/upload_helper");
const Constants = require("../utils/constants");
const PusherHelper = require("../utils/pusher_helper");
const pusherObj = new PusherHelper(Constants.getDashboardPusherConfig());



/** For uploading file */
const createItemObj = (item) => {
    let currentItem = {
        item_name: item.item_name,
        item_shelf_id: item.item_shelf_id,
        item_shelf_side: item.item_shelf_side,
        id: item._id
    }
    return currentItem;
}

module.exports.getSimulatorHome = function (request, response) {
    let items_arr = [];
    let locals = { "title": "Simulator Home", "items_arr": items_arr };

    CabinetItemModel.find({}, function (error, items) {
        if (error) {
            console.log("Error in fetching items from DB");
            return;
        }
        for (let item of items) {
            let currentItem = createItemObj(item, true);
            items_arr.push(currentItem);
        }
        return response.render("simulator", locals);
    });
}

// helper function to set file desitnation in fileProperties object to be used later for creating entry in the DB
function setFileDestination(shelf_id, fileProperties) {
    fileProperties["destination"] = UploadHelper.getFileStoragePath(shelf_id);
    return fileProperties["destination"];
}

// helper function to set file name in fileProperties object to be used later for creating entry in the DB
function setFileProperties(fieldname, fileSuffix, file_extension, fileProperties) {
    fileProperties["file_name"] = `${fieldname}-${fileSuffix}${file_extension}`;
    return fileProperties["file_name"];

}

// controller for /add-item action
module.exports.addItem = function (request, response) {

    // TODO: check if the item entered is unique - based on location say
    let fileSuffix = UploadHelper.getStorageFileSuffix();
    let fileProperties = {
    }

    // set storage engine with desitnation and filename - TODO: add limits like file-size
    const storageEngine = multer.diskStorage({
        destination: function (request, file, callback) {
            callback(null, setFileDestination(request.body.item_shelf_id, fileProperties))
        },
        filename: function (request, file, callback) {
            callback(null, setFileProperties(file.fieldname, fileSuffix, path.extname(file.originalname), fileProperties));
        }
    });

    // Initialize Upload variable
    const upload = multer({
        storage: storageEngine,
        limits: {
            fileSize: Constants.getImageFileUploadSizeLimit() // in bytes
        },
        fileFilter: function (request, file, callback) {
            UploadHelper.checkImageFileType(file, callback);
        }
    }).single(Constants.getImageFieldName());

    // call upload method
    upload(request, response, (error) => {

        if (error) {
            console.log("error", error);
            response.status(400);
            return response.send(error);
        }
        else {
            if (request.file == undefined) {
                response.status(400);
                return response.send("No File Selected");
            }
            else {
                CabinetItemModel.create({
                    item_name: request.body.item_name,
                    item_shelf_id: request.body.item_shelf_id,
                    item_shelf_side: request.body.item_shelf_side,
                    item_image_path: path.join(fileProperties["destination"], fileProperties["file_name"])
                }, function (error, newItem) {
                    if (error) {
                        console.log("Error creating model", error)
                        response.status(400);
                        return response.send(error);
                    }
                    else {
                        pusherObj.triggerMessage(Constants.getDashboadPusherChannelName(), Constants.getDashboardPusherEventName(), newItem);
                        let currentItem = createItemObj(newItem);
                        response.status(200);
                        return response.json(currentItem);
                    }
                });
            }
        }
    });
}


module.exports.deleteItems = function (request, response) {

    // create ids array to delete multiple items based on id
    let ids = [];
    for (let item of request.body.data) {
        ids.push(item.id);
    }

    let deletedIds = {
        _id: { $in: ids }
    };

    // delete multiple items
    CabinetItemModel.deleteMany(deletedIds, function (error, result) {
        if (error) {
            console.log("Failed to delete the records from the DB");
        }
        else {
            pusherObj.triggerMessage(Constants.getDashboadPusherChannelName(), Constants.getDashboardPusherEventName(), deletedIds);
        }
    });
    return response.redirect("back");
}