const express = require("express");
const CabinetShelfModel = require("../models/cabinet_shelf_model")

function extractItemInformation(allItems) {
    let itemsInfo = {}
    let shelfs = new Set();

    for (let item of allItems) {
        shelfs.add(item.shelf_id);
        if (!(item.shelf_id in itemsInfo)) {
            itemsInfo[item.shelf_id] = {};
        }
    }

    for (let item of allItems) {
        let currentId = item.shelf_id;

        if (!(item.shelf_side in itemsInfo[currentId])) {
            itemsInfo[currentId][item.shelf_side] = [];
        }
        itemsInfo[currentId][item.shelf_side].push(item._id);
    }
    return itemsInfo;
}

module.exports.getHome = function (request, response) {
    let locals = {};

    CabinetShelfModel.find({}, function (error, items) {
        if (error) {
            console.log(`Error in querying items from the db: ${error}`);
            return undefined;
        }
        let itemsInfo = extractItemInformation(items);
        locals["items_info"] = itemsInfo;
        return response.render("dashboard", locals);
    });
}

