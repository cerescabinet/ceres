const express = require("express");
const db = require("./config/mongoose");
var bodyParser = require('body-parser');
const port = 8000;
const app = express();
const expressRouter = require("./routes");


app.use(express.urlencoded());

/** setup views and view engine */
app.set("view engine", "ejs");
app.set("views", "./views");

/** parsing */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/** setup assets folder */
app.use(express.static("assets"));

/** route all routes to expressRouter */
app.use("/", expressRouter);


app.listen(port, function (err) {
    if (err) {
        console.log(`Failed to run the server: ${err}`);
        return;
    }
    console.log(`Backend Server running successfully on port: ${port}`)
});