const mongoose = require("mongoose");

const schemaObj = {
    shelf_id: {
        type: String,
        required: true
    },

    shelf_side: {
        type: String,
        enum: ["left", "right"],
        required: true
    },

    image_path: {
        type: String,
        required: true
    }

    /**TODO: itemCoordinates */
}

/** Create schema */
const cabinetShelfSchema = new mongoose.Schema(schemaObj);

/** Compiling schema into a model */
const CabinetShelf = mongoose.model("Cabinet_Shelf", cabinetShelfSchema);

module.exports = CabinetShelf;