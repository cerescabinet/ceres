const mongoose = require("mongoose");

const schemaObj = {
    item_id: {
        type: String,
        required: true
    },
    item_shelf_id: {
        type: String,
        required: true
    },
    item_category: {
        type: String
    },
    item_brand: {
        type: String
    },
    item_side: {
        type: String,
        required: true
    },
    // SI unit - gm
    item_full_weight: {
        type: Number  // TODO - check if this needs to be an object
    },
    item_current_weight: {
        type: Number   // TODO - check if this needs to be an object
    },
    item_location: [
        {
            x: {
                type: Number
            },
            y: {
                type: Number
            },
            h: {
                type: Number
            },
            w: {
                type: Number
            }
        }],
}

/** Create schema */
const cabinetItemSchema = new mongoose.Schema(schemaObj);

/** Compiling schema into a model */
const CabinetItem = mongoose.model("Cabinet_Item", cabinetItemSchema);

module.exports = CabinetItem;