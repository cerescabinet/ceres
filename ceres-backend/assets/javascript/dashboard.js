// Enable pusher logging - don't include this in production
// Pusher.logToConsole = true;

// Initialize Pusher
const pusher = new Pusher('806539a7dfcc5ce27dd9', {
    cluster: 'us2',
    forceTLS: true
});

// Initialize Channel
const channel = pusher.subscribe("dashboard-channel");

// Bind Channel to subscribe to event
channel.bind("dashboard-event", function (data) {
    let shelf_id = data.message.shelf_id;
    let shelf_side = data.message.shelf_side;
    let image_path = `/images/shelf/shelf_${shelf_id}_${shelf_side}.jpg`;
    updateImage(shelf_id, shelf_side, image_path)
});

// helper function to update shelf-side image when it changes
function updateImage(shelf_id, shelf_side, image_path) {
    console.log("this function is called");
    console.log(shelf_id, shelf_side);
    let query = `#${shelf_id}-${shelf_side}`;
    $(query).find("img").attr("src", image_path + '?' + new Date().getTime());
}


// on loading the page --> get all items and set coordiantes for each item in an image

window.onload = function () {
    $.get("/get-all-items", function (data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            for (let item of data) {
                if (globalItemsObj[item.item_shelf_id] == undefined) {
                    globalItemsObj[item.item_shelf_id] = [];
                }
                globalItemsObj[item.item_shelf_id].push(item);

            }
            console.log(globalItemsObj);
            createImageMap(globalItemsObj);
        }
        else {
            console.log(`Could not get all items: ${jqXHR.status}`)

        }
    });
};


let globalItemsObj = {};

function createTitle(item) {
    let arr = { "item_brand": "Brand", "item_category": "Category", "item_current_weight": "Current Weight", "item_full_weight": "Full Weight" };
    let stringArr = [];
    for (const [key, value] of Object.entries(arr)) {
        if (item[key] != undefined || item[key] == "") {
            stringArr.push(`${value}: ${item[key]}`)
        }
    }
    return stringArr.join("\n");
}

function createImageMap(itemsObj) {
    for (const [shelf_id, items] of Object.entries(itemsObj)) {
        for (let item of items) {
            //$(`#${shelf_id}-${item.item_side}`).tooltip();
            let mapQuery = `map[name="${shelf_id}-${item.item_side}-map"]`
            let element = $(mapQuery);
            let mapId = `${shelf_id}-${item.item_side}-map`;
            /**
            let imgQuery = `#shelf-${shelf_id}-${item.item_side}-image`;
            let imageElement = $(imgQuery).clone().appendTo("body").css({ 'display': 'block', 'visibility': 'visible' });
            var theWidth = imageElement.width();
            let theHeight = imageElement.height()
            imageElement.remove();
            console.log("IMAGE ELEMENT", theWidth, theHeight);

            //console.log("IMAGE ELEMENT", imgQuery, imageElement.outerWidth(), imageElement.outerHeight())
             */

            // if map tag does not exist for this image
            if (element.length == 0) {
                // add map tag 
                let string = `<map name="${shelf_id}-${item.item_side}-map" id=${mapId}></map>`;
                let divQuery = `#${shelf_id}-${item.item_side}`;
                $(divQuery).append(string);

                // join the image with its map tag
                $(`#shelf-${shelf_id}-${item.item_side}-image`).attr("usemap", `${divQuery}-map`);
            }
            // add the area tag to the map
            let location = item.item_location;
            let coordinates = `${location[0].y / 2.95},${location[0].x / 2.95},${(location[0].w + location[0].y) / 2.95},${(location[0].h + location[0].x) / 2.95}`;

            console.log(item.item_category, coordinates, location[0].w / 2.95, location[0].h / 2.95);
            // let areaString = `<area shape="rect" coords="${coordinates}" alt="Computer" href="">`;
            // $(`#${mapId}`).append(areaString);
            $(`#${mapId}`).append(
                $("<area></area>")
                    .attr({
                        "data-html": true,
                        shape: "rect",
                        coords: coordinates,
                        title: createTitle(item), //`<b>${item.item_brand}\n${item.item_category}</b>`,
                        href: "",
                        id: `area - ${item.item_id}`
                    })
            );
        }
    }

}


