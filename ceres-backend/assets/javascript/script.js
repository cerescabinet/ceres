function addListItem(item) {
    let string = `<tr class="shelf-item-list-row"><td class="shelf-item-list-cell" id=${item.id}>
                <label class="shelf-item-label" for=shelf-item-${item.id}><input type="checkbox" class="shelf-item-obj" name="content"
                id=shelf-item-${item.id}>Shelf ${item.shelf_id} Side ${item.shelf_side}</label>
                </td></tr>`;

    $("#shelf-item-table").append(string);
}

function clearFields() {
    $('input[name="shelf_id"]').prop('checked', false);
    $('input[name="shelf_side"]').prop('checked', false);
}

function addItem() {
    let shelfId = $("input[type='radio'].shelf_id:checked").val();
    let filePath = $("#fs_browser").val().trim();
    let shelfSide = $("input[type='radio'].shelf_side:checked").val();
    let file = $("#fs_browser")[0].files[0];


    if (shelfId == null) {
        window.alert("Select a shelf id for the shelf item");
        return;
    }
    else if (shelfSide == null) {
        window.alert("Select a shelf side for the shelf item");
        return;
    }
    else if (filePath == "") {
        window.alert("Choose an image file for the shelf item");
        return;
    }

    // let formData = new FormData($("form")[0]);
    let formData = new FormData();
    formData.append("shelf_id", shelfId);
    formData.append("shelf_side", shelfSide);
    formData.append("imageUpload", file);

    /**
    $.post("/add-item", { "item_name": inputText, "item_shelf_id": shelfId, "file_path": filePath }, function (responseData) {
        console.log("response data");
    });
     */

    $.ajax({
        type: "POST",
        url: "/add-shelf-item",
        data: formData,
        processData: false,
        contentType: false,
        success: function (response, textStatus, jqXHR) {
            console.log(`Response code received from server: ${jqXHR.status}`)
            if (jqXHR.status == 200) {
                console.log(response);
                addListItem(response);
            }
            else {
                console.log(response);
            }
        }
    });
    clearFields();

}

function deleteShelfItems() {

    let labelList = $(".shelf-item-label");
    let itemsArr = prepareShelfItemsList(labelList);

    console.log(itemsArr);

    $.post("/delete-shelf-items", { "data": itemsArr }, function (responseData) {
        removeElements(labelList);
    });
}



function prepareShelfItemsList(labelList) {
    let itemsArr = [];
    for (let label of labelList) {
        let query = `#${label.htmlFor}`;
        if ($(query)[0].checked) {
            let textContent = label.textContent;
            let id = $(query)[0].id.slice("shelf-item-".length);
            itemsArr.push({ "itemName": textContent, "id": id });
        }
    }
    return itemsArr;
}

function removeElements(labelList) {
    for (let label of labelList) {
        let query = `#${label.htmlFor}`;
        if ($(query)[0].checked) {
            $(label).closest(".shelf-item-list-row").remove();
        }
    }
}

$("#add-shelf-item").click(addItem);

$("#delete-shelf-items").click(deleteShelfItems);