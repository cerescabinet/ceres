const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();

/** Setup middleware to parse client data */
//router.use(express.urlencoded());

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(express.static("../assets/images"));
console.log("Router is loaded inside /router/index.js");

/** import controllers */
const homeController = require("../controllers/home_controller");
const simulatorController = require("../controllers/simulater_controller");
const shelfController = require("../controllers/shelf_controller");

/** setup router to route URIs to respective controller */
router.get("/", homeController.getHome);

// get information of all items present in the DB
router.get("/get-all-items", shelfController.getAllItems);

// to get information of items in a specific shelfId-shelfSide
router.get("/get-image-item-info", shelfController.getImageItemInfo);

router.get("/simulator", shelfController.getSimulatorHome);

// to update shelf image
router.post("/add-shelf-item", shelfController.addShelfItem);
router.post("/delete-shelf-items", shelfController.deleteShelfItems);

// to add / update item information in the DB
router.post("/add-items", shelfController.addItems);

module.exports = router;
