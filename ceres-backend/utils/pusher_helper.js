const Pusher = require('pusher');


/**
var pusher = new Pusher({
    appId: '963066',
    key: 'b1f82683195c22ffee7f',
    secret: 'bfa6d647f37858a26faa',
    cluster: 'us2',
    encrypted: true
});

pusher.trigger('my-channel', 'my-event', {
    "message": "hello world"
});*/


module.exports = class PusherHelper {

    constructor(pusherConfig) {
        this.pusher = new Pusher({
            appId: pusherConfig.appId,
            key: pusherConfig.key,
            secret: pusherConfig.secret,
            cluster: pusherConfig.cluster,
            encrypted: pusherConfig.encrypted
        });
    }

    triggerMessage(channelName, eventName, message) {
        this.pusher.trigger(channelName, eventName, {
            "message": message
        });

        console.log("pusher triggered the message");
    }
}


