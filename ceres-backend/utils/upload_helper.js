const path = require("path");
const multer = require("multer");

const Constants = require("./constants");
const PathHelper = require("./path_helper");

class UploadHelper {

    static getStorageFileSuffix() {
        return `${Date.now()}`;
    }

    static getFileStoragePath(folderName) {

        let targetDir = path.join(Constants.getImageOutputFolder(), folderName);
        console.log("targetdir", targetDir);
        let status = PathHelper.createDirPathSync(targetDir);
        console.log(status)
        if (status) {
            return targetDir;
        }

        return undefined;
    }

    static getStorageFileName(file) {
        let storageFileName = `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`;
        console.log(`Storage File name for file: ${file.originalname}: ${storageFileName}`);
        return storageFileName;
    }

    // checking image file type - both extension and MIME type
    static checkImageFileType(file, callback) {
        const filetypes = /jpeg|jpg|png|gif/;
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        /** TODO: find way to upload images with correct mimetype using Python requests */
        const mimetype = true; //filetypes.test(file.mimetype);

        console.log("MIMETYPE", mimetype, file.mimetype);
        console.log("EXTNAME", extname);
        if (mimetype && extname) {
            console.log("true status")
            return callback(null, true)
        }
        console.log("not true");
        return callback("Error: Only images can be uploaded!");
    }
}

module.exports = UploadHelper;
