const path = require("path");

class Constants {

    static getRootFolder() {
        return path.dirname(require.main.filename);
    }

    static getImageOutputFolder() {
        //return path.join(Constants.getRootFolder(), "s3");
        return path.join(Constants.getRootFolder(), "/assets/images");
    }

    static getImageFileUploadSizeLimit() {
        // bytes
        return 1000000;
    }

    static getImageFieldName() {
        return "imageUpload";
    }

    static getDashboadPusherChannelName() {
        return "dashboard-channel";
    }

    static getDashboardPusherEventName() {
        return "dashboard-event";
    }

    static getDashboardPusherConfig() {
        return {
            appId: '966331',
            key: '806539a7dfcc5ce27dd9',
            secret: '5d354bc61088f91ce63f',
            cluster: 'us2',
            encrypted: true
        }
    }
}

module.exports = Constants;