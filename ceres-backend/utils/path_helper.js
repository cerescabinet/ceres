const fs = require("fs");

class PathHelper {

    static createDirPathSync(target_dir) {
        try {
            fs.mkdirSync(target_dir, { recursive: true });
            return true;
        }
        catch (err) {
            if (err.code === "EEXIST") {
                console.log(`Directory already exists: ${target_dir}`)
                return true;
            }
            else {
                throw new Error(`Error creating the directory: ${target_dir} - ${err}`)
            }

        }
    }
}

module.exports = PathHelper;
