#include "FastMap.h"
#include <SPI.h>
#include "FastGPIO.h"

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit)) // Macro to clear a bit
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit)) // Macro to set a bit

FastMap mapper; // A faster map() function

const int latchPin = 4; //12; // Shift Register controls
const int clockPin = 3; //11; // Shift Register controls
const int dataPin = 2;  //10;  // Shift Register controls

int inputVal = 1;
// MUX
int SIG_pin = 0;
int s0 = 9;
int s1 = 8;
int s2 = 7;
int s3 = 6;

void setup() {
  // MUX
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);
  pinMode(s3, OUTPUT);
  
  digitalWrite(s0, LOW);
  digitalWrite(s1, LOW);
  digitalWrite(s2, LOW);
  digitalWrite(s3, LOW);

  pinMode(latchPin, OUTPUT);
  digitalWrite(latchPin, HIGH);
  pinMode(dataPin, OUTPUT);
  digitalWrite(dataPin, LOW);
  pinMode(clockPin, OUTPUT);
  digitalWrite(clockPin, HIGH);
  
  Serial.begin(500000);
  delay(100);
  Serial.write(0); // 0 indicates start of new frame
//  /mapper.init(0, 1023, 1, 255);
}

void loop() {
  for (int row=0; row<16; row++) // Changing rows is slow, so it's the outer loop
  {
      FastGPIO::Pin<latchPin>::setOutput(LOW);  // Send 1 high bit to the shift register chain
      FastGPIO::Pin<dataPin>::setOutput(HIGH);  // Send 1 high bit to the shift register chain
      FastGPIO::Pin<clockPin>::setOutput(LOW);  // Send 1 high bit to the shift register chain
      //delayMicroseconds(2);
      FastGPIO::Pin<clockPin>::setOutput(HIGH); // Send 1 high bit to the shift register chain
      //delayMicroseconds(2);
      FastGPIO::Pin<clockPin>::setOutput(LOW);  // Send 1 high bit to the shift register chain
      FastGPIO::Pin<dataPin>::setOutput(LOW);   // Send 1 high bit to the shift register chain
      FastGPIO::Pin<latchPin>::setOutput(HIGH); // Send 1 high bit to the shift register chain
      //delayMicroseconds(2);
      
      for (int col=0; col< 32; col++) // Changing cols is fast, so it's the inner loop
      {
        inputVal = readMux(row);
        if (col < 16){          
//          Serial.write((byte)mapper.map(inputVal)); // Actually write 1 byte to serial/
          Serial.write((byte)inputVal); // Actually write 1 byte to serial
//          Serial.print(inputVal);
//          Serial.print(" ");
        }

        FastGPIO::Pin<latchPin>::setOutput(LOW);  // Shift shift register data 1 step
        FastGPIO::Pin<clockPin>::setOutput(HIGH); // Shift shift register data 1 step
        FastGPIO::Pin<latchPin>::setOutput(HIGH); // Shift shift register data 1 step
        FastGPIO::Pin<clockPin>::setOutput(LOW);  // Shift shift register data 1 step
      }
      Serial.write((byte)row); // Actually write 1 byte to serial/
//      Serial.println("");
//      delay(100);
  }
//  Serial.write(0); // Mark the beginning of the next frame
}

int readMux(int channel){ 
  int controlPin[] = {s0, s1, s2, s3}; 
  int muxChannel[16][4]={ {0,0,0,0}, {1,0,0,0}, {0,1,0,0}, {1,1,0,0}, {0,0,1,0}, {1,0,1,0}, {0,1,1,0}, {1,1,1,0}, {0,0,0,1}, {1,0,0,1}, {0,1,0,1}, {1,1,0,1}, {0,0,1,1}, {1,0,1,1}, {0,1,1,1}, {1,1,1,1} }; 
  
  //loop through the 4 sig 
  for(int i = 0; i < 4; i ++){ 
    digitalWrite(controlPin[i], muxChannel[channel][i]); 
  } 
  
  //read the value at the SIG pin 
  int val = analogRead(SIG_pin); 
  
  //return the value 
  return val; 
}
