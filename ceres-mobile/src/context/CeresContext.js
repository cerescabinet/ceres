import React, { useState } from "react";
const CeresContext = React.createContext();

// this will server as the parent component for all components
export const CeresProvider = ({ children }) => {
  const [sampleData, setData] = useState("");

  /**const socketObj = SocketUtils.getSocket().on("some-channel", function (data) {
    console.log("some new data received in shelf item", data);
    setData(data);
  });**/

  return <CeresContext.Provider value={{ data: sampleData }}>{children}</CeresContext.Provider>;
};

export default CeresContext;
