import Constants from "./Constants";
import io from "socket.io-client";

export const socket = io(Constants.getNgrokUrl());
