import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import ShelfSideComponent from "../components/ShelfSideComponent";
import { socket } from "../utils/socket";

const HomeScreen = ({ navigation }) => {
  useEffect(() => {
    console.log(socket.connected, "on home screen");

    socket.on("some-channel", function (data) {
      console.log("some new data received in shelf item", data);
    });
  });
  return (
    <View>
      {/* ROW 1 */}
      <View style={styles.container}>
        <View>
          <ShelfSideComponent shelfId="1" shelfSide="left" />
        </View>
        <View>
          <ShelfSideComponent shelfId="1" shelfSide="right" />
        </View>
      </View>

      {/* ROW 2 */}
      <View style={styles.container}>
        <View>
          <ShelfSideComponent shelfId="2" shelfSide="left" />
        </View>
        <View>
          <ShelfSideComponent shelfId="2" shelfSide="right" />
        </View>
      </View>

      {/* ROW 3 */}
      <View style={styles.container}>
        <View>
          <ShelfSideComponent shelfId="3" shelfSide="left" />
        </View>
        <View>
          <ShelfSideComponent shelfId="3" shelfSide="right" />
        </View>
      </View>
    </View>
  );
};

const styleObj = {
  container: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-around",
  },
};

const styles = StyleSheet.create(styleObj);

export default HomeScreen;
